# ros-hpc
## Getting started
Clone the repository into your home directory on HPC and navigate to the cloned directory.

```
git clone https://bitbucket.org/pepper_qut/ros-hpc
cd ros-hpc
```

Note: Feel free to choose another location, I will just assume the root of your home directory for simplicity.

## HPC Modules
At the top of the build script are a number of system dependencies needed to compile and run ROS. The versions are arbitrary, so please feel free to change the versions to suite your use case. For ease of use, default versions have been defined, which are as follows:
```
python/2.7.11-foss-2016a
ffmpeg/3.0.2-foss-2016a
opencv/3.1.0-foss-2016a
boost/1.58.0-foss-2016a-python-2.7.11
cmake/3.5.2-foss-2016a
libyaml
```

Note: I do not use HPC regularly, and will therefore not provide any recommendations on module versions.

## Building ROS
To build ROS simply run the build script, either as a job, or in an interactive session. ***Do not run run this command on the head node!*** 

```
./build
```

This will build a light-weight version of ROS that gives you access to rospy, roscpp, many message types, and importantly, the ability to parse bag files within your python or C++ projects.

To make use of your ROS installation, you now need to source the ROS setup.bash script. Assuming you are cloned ros-hpc into your home directory, you simply need to run the command:

```
source ~/ros-hpc/ros_toolchain_install/setup.bash
```

To make the ROS toolset available between logins, add the previous command to the bottom of your .bashrc script.

## Building Additional Packages

If you wish to build additional ROS packages, you can now create a catkin workspace as usual (see the ROS documentation if you are unfamiliar with this process)

